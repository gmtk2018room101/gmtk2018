﻿using UnityEngine.Audio;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;


[System.Serializable]
public class Sound
{
    public string name;
    public AudioClip audioClip;

    [Range(0f, 1f)]
    public float volume;

    [Range(0f, 3f)]
    public float pitch;

    public AudioSource source;

    public bool loop;
}

public class AudioManager : MonoBehaviour
{

    public Sound[] sounds;

    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);

        for (int i = 0; i < this.sounds.Length; i++)
        {
            Sound s = this.sounds[i];
            s.source = this.gameObject.AddComponent<AudioSource>();
            s.source.clip = s.audioClip;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
        }
        if(SceneManager.GetActiveScene().name == "Level1")
            this.Play("AmbientIntro", "AmbientLoop");
    }

    public void Play(string soundName, string nextSoundName=null)
    {
        Sound s = Array.Find<Sound>(this.sounds, sound => sound.name == soundName);
        s.source.Play();
        if(nextSoundName != null)
        {
            Sound sNext = Array.Find<Sound>(this.sounds, sound => sound.name == nextSoundName);
            sNext.source.PlayScheduled(s.source.clip.length);
        }
    }

    public void Stop(string soundName)
    {
        Sound s = Array.Find<Sound>(this.sounds, sound => sound.name == soundName);
        s.source.Stop();
    }

    public void Silence()
    {
        foreach(Sound s in this.sounds)
        {
            s.source.Stop();
        }
    }
}
