﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EyeBoyBehaviour : MonoBehaviour {
    public NavMeshAgent NavMeshAgent;

    private GameObject player;
    private FieldOfView[] allFieldsOfView;
    private bool _hasTarget;
    public SpriteRenderer spriteRenderer;
    private AudioManager audioManager;

    // Use this for initialization
    void Start()
    {
        this.player = GameObject.FindWithTag("Player");
        this.allFieldsOfView = FindObjectsOfType<FieldOfView>();
        this.audioManager = FindObjectOfType<AudioManager>();
    }

    void Update()
    {
        this._hasTarget = false;
        // check if inside any enabled field of views.
        foreach (FieldOfView fov in this.allFieldsOfView)
        {
            fov.FindVisibleTargets();
            // hope there's never more than one target lol
            foreach (Transform target in fov.visibleTargets)
            {
                if (target.gameObject != this.gameObject)
                {
                    continue;
                }
                this._hasTarget = true;
                if(this.NavMeshAgent.isStopped)
                {
                    this.audioManager.Play("Spotted");
                }
                this.MoveToLocation(player.transform.position);
                break;
            }
        }
        
        if (!this._hasTarget)
        {
            this.NavMeshAgent.isStopped = true;
        }

        // flip the sprite based on which way he's movin'
        this.spriteRenderer.flipX = this.NavMeshAgent.velocity.x > 0;
    }

    public void MoveToLocation(Vector3 target)
    {
        this.NavMeshAgent.destination = target;
        this.NavMeshAgent.isStopped = false;
    }
}
