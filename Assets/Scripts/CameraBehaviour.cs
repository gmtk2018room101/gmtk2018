﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBehaviour : MonoBehaviour {

    public GameObject Player;
    private float originalZCoordinate;
    public float dampTime = 0.03f;
    private Vector3 velocity = Vector3.zero;
    public CameraState state = CameraState.unMoving;

    public enum CameraState
    {
        unMoving = 0,
        followingPlayer = 1,
    }

    // Use this for initialization
    void Start () {
        this.originalZCoordinate = this.transform.position.z;
	}
	
	// Update is called once per frame
	void Update () {
        if(this.state == CameraState.followingPlayer)
        {
            Vector3 playerVector = new Vector3(this.Player.transform.position.x, this.Player.transform.position.y, this.originalZCoordinate);
            this.transform.position = Vector3.SmoothDamp(this.transform.position, playerVector, ref this.velocity, this.dampTime);
            //this.transform.position = new Vector3(this.Player.transform.position.x, this.Player.transform.position.y, this.transform.position.z);
        }
    }
}
