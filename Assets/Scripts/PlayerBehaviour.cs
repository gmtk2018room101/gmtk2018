﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerBehaviour : MonoBehaviour {

	public Animator animator;
    public LayerMask clickLayerMask;
    public GameObject blinkTarget;
    private FieldOfView[] allFieldsOfView;
    public float BlinkCooldown;
    public float BlinkWarmUpTime;
    public float BlinkWarmDownTime;
    public float TimeToTransitionToNextLevel;
    public Rigidbody rigidBody;
    public LevelManagerBehavuour levelManagerBehavuour;
    public TorchBehaviour torchBehaviour;
    public CameraBehaviour cameraBehaviour;
    public Animator cameraFadeAnimator;

    private AudioManager audioManager;
    private Vector3 startPosition;
    private float timeOfLastBlink = 0f;
    public playerState state = playerState.falling;
    private float timeOfBlinkStart = 0f;
    private float timeOfBlink = 0f;
    private Vector3 targetOfBlink;
    private GameObject[] allObstacles;
    private float timeStartedTransitioningToNextLevel;
    private string nextScene;

    public enum playerState
    {
        falling = 0,
        idle = 1,
        blinkingIntoGem = 2,
        blinkingOutOfGem = 3,
        transitioningToNextLevel = 4,
    }

    // Use this for initialization
    void Start () {
        this.audioManager = FindObjectOfType<AudioManager>();
        this.startPosition = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
        this.timeOfLastBlink = Time.time;
        this.allFieldsOfView = FindObjectsOfType<FieldOfView>();
	}
	
	// Update is called once per frame
	void Update () {

        if (this.state == playerState.transitioningToNextLevel)
        {
            if (Time.time - this.timeStartedTransitioningToNextLevel > this.TimeToTransitionToNextLevel)
            {
                Debug.LogFormat("Loading Stage {0}", this.nextScene);
                SceneManager.LoadScene(this.nextScene);
            }
            return;
        }

        if (this.allObstacles == null)
        {
            this.allObstacles = GameObject.FindGameObjectsWithTag("Obstacle");
            Debug.LogFormat("Numer of Obstacles: {0}", this.allObstacles.Length);
        }

        this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, -0.26f);

        bool blinkHappenedThisFrame = false;
        if (Input.GetMouseButtonDown(0) && this.state == playerState.idle)
        {
            // TODO move to its own fxn
            if (Time.time - this.timeOfLastBlink > this.BlinkCooldown)
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit))//, Mathf.Infinity, clickLayerMask))
                {
                    // TODO: checks to ensure the player doesn't click too close to a platform and fall through it.
                    Vector3 newBlinkTarget = new Vector3(hit.point.x, hit.point.y, this.blinkTarget.transform.position.z);
                    bool tooCloseToWall = false;

                    // Check every wall in the scene - if too close to any, don't continue
                    foreach (GameObject obstacle in this.allObstacles)
                    {
                        if (Vector3.Distance(obstacle.transform.position, newBlinkTarget) < 1.0)
                        {
                            Debug.LogFormat("Position {0} is too close to wall {1}. Fizzling.",
                                newBlinkTarget.ToString(), obstacle.ToString());
                            tooCloseToWall = true;
                            break;
                        }
                    }

                    if (!tooCloseToWall)
                    {
                        this.blinkTarget.transform.position = newBlinkTarget;

                        foreach (FieldOfView fov in this.allFieldsOfView)
                        {
                            fov.FindVisibleTargets();
                            // hope there's never more than one target lol
                            foreach (Transform target in fov.visibleTargets)
                            {
                                if (target.gameObject.name != "BlinkTarget")
                                {
                                    continue;
                                }
                                Debug.LogFormat("Found something to blink to! {0}", target.gameObject.name);

                                blinkHappenedThisFrame = true;
                                this.targetOfBlink = new Vector3(target.gameObject.transform.position.x, target.gameObject.transform.position.y, this.transform.position.z);
                                // we're done with the blinktarget for now, fling it out of existence
                                target.position = new Vector3(10000f, 10000f, target.position.z);
                                this.timeOfLastBlink = Time.time;
                                this.audioManager.Play("Disappear");
                            }
                            if (blinkHappenedThisFrame)
                            {
                                break;
                            }
                        }
                    }
                }
            }
        }

        if(blinkHappenedThisFrame)
        {
            this.state = playerState.blinkingIntoGem;
            this.timeOfBlinkStart = Time.time;
            //this.audioManager.Play("Appear");
        }

        if (this.state == playerState.blinkingIntoGem && (Time.time - this.timeOfBlinkStart > this.BlinkWarmUpTime))
        {
            this.state = playerState.blinkingOutOfGem;
            this.transform.position = this.targetOfBlink;
            this.timeOfBlink = Time.time;

        }
        
        if (this.state == playerState.blinkingOutOfGem && (Time.time - this.timeOfBlink > this.BlinkWarmDownTime))
        {
            this.state = playerState.falling;
            this.rigidBody.useGravity = true;
        }

        // update the player's animation enum based on the current state
        this.animator.SetInteger("PlayerState", (int)this.state);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (this.state == playerState.transitioningToNextLevel)
        {
            return;
        }
        switch (other.gameObject.tag)
        {
            case "Obstacle":
                this.state = playerState.idle;
                this.rigidBody.useGravity = false;
                this.rigidBody.velocity = Vector3.zero;
                this.transform.position = new Vector3(this.transform.position.x, other.gameObject.transform.position.y + 1.2f, this.transform.position.z);
                this.audioManager.Play("Fall");
                break;
            case "Enemy":
                if(this.state != playerState.blinkingIntoGem)
                {
                    this.levelManagerBehavuour.ResetLevel();
                }
                break;
            case "StoryCollider1":
                this.torchBehaviour.Light();
                break;
            case "StoryCollider2":
                this.audioManager.Stop("AmbientLoop");
                this.audioManager.Play("MainFull");
                this.cameraBehaviour.state = CameraBehaviour.CameraState.followingPlayer;
                break;
            case "StoryCollider3":
                this.rigidBody.useGravity = false;
                this.rigidBody.velocity = Vector3.zero;
                this.state = playerState.transitioningToNextLevel;
                this.timeStartedTransitioningToNextLevel = Time.time;
                this.nextScene = "Chase";
                this.cameraFadeAnimator.SetBool("FadeIn", true);
                break;
            case "StoryCollider4":
                this.rigidBody.useGravity = false;
                this.rigidBody.velocity = Vector3.zero;
                this.state = playerState.transitioningToNextLevel;
                this.timeStartedTransitioningToNextLevel = Time.time;
                this.nextScene = "Labyrinth";
                this.cameraFadeAnimator.SetBool("FadeIn", true);
                break;
            case "StoryCollider5":
                this.rigidBody.useGravity = false;
                this.rigidBody.velocity = Vector3.zero;
                this.state = playerState.transitioningToNextLevel;
                this.timeStartedTransitioningToNextLevel = Time.time;
                this.nextScene = "GameOverWin";
                this.cameraFadeAnimator.SetBool("FadeIn", true);
                break;
            default:
                break;
        }

    }
}
