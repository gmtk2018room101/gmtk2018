﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FryBoyBehaviour : MonoBehaviour {

    public GameObject patrolPointOne;
    public GameObject patrolPointTwo;
    public NavMeshAgent NavMeshAgent;
    public float TimeSpentOnEachRoute;
    public SpriteRenderer spriteRenderer;

    private float timeStartedCurrentRoute;
    private GameObject currentTarget;

	// Use this for initialization
	void Start () {
        this.timeStartedCurrentRoute = Time.time;
        this.currentTarget = this.patrolPointOne;
	}
	
	// Update is called once per frame
	void Update () {
        if((Time.time - this.timeStartedCurrentRoute) > this.TimeSpentOnEachRoute)
        {
            if(this.currentTarget == this.patrolPointOne)
            {
                this.currentTarget = this.patrolPointTwo;
            }
            else if (this.currentTarget == this.patrolPointTwo)
            {
                this.currentTarget = this.patrolPointOne;
            }
            this.timeStartedCurrentRoute = Time.time;
        }
        this.MoveToLocation(this.currentTarget.transform.position);
        
        // flip the sprite based on which way he's movin'
        this.spriteRenderer.flipX = this.NavMeshAgent.velocity.x > 0;
    }

    public void MoveToLocation(Vector3 target)
    {
        this.NavMeshAgent.destination = target;
        this.NavMeshAgent.isStopped = false;
    }
}
