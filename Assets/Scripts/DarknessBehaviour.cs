﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DarknessBehaviour : MonoBehaviour {
    public MeshRenderer meshRenderer;

	// Use this for initialization
	void Start () {
        this.meshRenderer.enabled = true;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
