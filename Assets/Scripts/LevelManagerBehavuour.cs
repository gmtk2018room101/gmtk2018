﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManagerBehavuour : MonoBehaviour {

    private AudioManager audioManager;

	// Use this for initialization
	void Start () {
        this.audioManager = FindObjectOfType<AudioManager>();
    }
	
	public void ResetLevel()
    {
        //reload the current scene
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        this.audioManager.Play("Death");
    }
}
