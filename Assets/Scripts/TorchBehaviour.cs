﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorchBehaviour : MonoBehaviour {

    public TorchState state;
    public FieldOfView fieldOfView;

    private AudioManager audioManager;
    private float fieldOfViewOriginalRadius;

    public enum TorchState
    {
        unlit = 0,
        lit = 1,
    }

	// Use this for initialization
	void Start () {
        this.audioManager = FindObjectOfType<AudioManager>();
        this.fieldOfViewOriginalRadius = fieldOfView.viewRadius;
        if (this.state == TorchState.unlit)
        {
            fieldOfView.viewRadius = 0;
        }
	}
	
	// Update is called once per frame
	void Update () {
        if(Input.GetKeyDown("l"))
        {
            this.Light();
        }
	}

    public void Light()
    {
        this.audioManager.Play("FireLights");
        this.state = TorchState.lit;
        this.fieldOfView.viewRadius = this.fieldOfViewOriginalRadius;
    }
}
